terraform {
  required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.48.0"
    }
  }
}

resource "openstack_compute_instance_v2" "flinkers" {
  count           = var.num_servers
  name            = "deployment-flink${count.index}"
  image_name      = var.image_name
  flavor_name     = var.flavor
  security_groups = ["default"]
  metadata = {
    owner = "Search Platform Team"
  }
  user_data = "{file(${path.module}/cloud-init.sh)}"
  network {
    name = "lan-flat-cloudinstances2b"
  }
}

resource "local_file" "ansible_inventory" {
  content = templatefile("${path.root}/ansible/hosts.tmpl", {
    flink_servers = openstack_compute_instance_v2.flinkers.*.name
    domain_suffix = "${local.dns_domain}"
    }
  )
  filename        = "ansible/hosts.ini"
  file_permission = "0644"
  # Sleep a few minutes so DNS can propagate, then run playbook. 
  provisioner "local-exec" {
    # Sleep a few minutes so DNS can propagate, then run playbook. 
    command = "sleep 300; ansible-playbook -i ansible/hosts.ini ansible/init.yml"
  }
}

resource "local_file" "ansible_inventory" {
  content = templatefile("${path.root}/ansible/hosts.tmpl", {
    flink_servers = openstack_compute_instance_v2.flinkers.*.name
    domain_suffix = "${local.dns_domain}"
    }
  )
  filename        = "ansible/hosts.ini"
  file_permission = "0644"
  provisioner "local-exec" {
    # Sleep a few minutes so DNS can propagate, then run playbook. 
    command = "sleep 300; ansible-playbook -i ansible/hosts.ini ansible/init.yml"
  }
}