provider "openstack" {
}

variable "num_servers" {
  type    = number
  default = 1
}

variable "owner" {
  type    = string
  default = ""
}

variable "flavor" {
  type    = string
  default = "g3.cores4.ram8.disk20"
}

variable "image_name" {
  type    = string
  default = "debian-11.0-bullseye"

}

variable "tenant_name" {
  type    = string
  default = ""
}


variable "wmcs_domain" {
  type    = string
  default = "eqiad1.wikimedia.cloud"

}
