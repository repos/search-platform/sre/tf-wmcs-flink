output "flink_servers" {
  value = [for k, v in openstack_compute_instance_v2.flinkers : "${v.name}.${local.dns_domain}"]

}