# TF-WMCS-FLINK

Deploys [Flink Operator Quickstart](https://nightlies.apache.org/flink/flink-kubernetes-operator-docs-main/docs/try-flink-kubernetes-operator/quick-start/ ) on WMCS.

## Prerequisites
The following should be installed on your computer before you start:
- Terraform
- Openstack CLI
- Ansible
This guide presumes that you're using a UNIX-like OS, and have some degree of familiarity with bash.

## How to use
1. Generate [Openstack API application credentials](https://wikitech.wikimedia.org/wiki/Help:Using_OpenStack_APIs#Application_Credentials).
2. From [the WMCS Horizon API access page](https://horizon.wikimedia.org/project/api_access), click "Download Openstack RC File" and select OpenStack RC File.
3. Save the file on your computer. It contains sensitive information, so be sure to set the correct permissions.
4. From your terminal, source the file. For example, if the file is called "nova.config", then type `source nova.conf` and hit Enter.
5. This should add a bunch of environment variables to your terminal session. Both Openstack CLI and the Terraform Openstack provider will automatically consume them. This means you don't need to add specific data about your cloud account, username, password, etc into your Terraform plans.
6. If you want to verify that the environment variables are set properly, run `openstack flavor list` from your terminal. You should get a response containing the flavors offered by the Openstack Compute API. If not, you may need to repeat step 4. You can check for the environment variables by typing `env | grep OS` from your terminal.
7. In your terminal, navigate to the root of this repository. Initialize the Terraform repository by typing `terraform init` and hitting Enter.
