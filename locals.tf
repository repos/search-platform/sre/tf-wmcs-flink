# combine the tenant name and wmcs domain to create the end of any WMCS host's FQDN

locals {
  dns_domain = "${var.tenant_name}.${var.wmcs_domain}"
}
